.. _about:

About
=====


.. _what-is-it:

What is BuildGrid?
------------------

BuildGrid is a Python remote execution service which implements Google's
`Remote Execution API`_ and the `Remote Workers API`_. This repository only
contains the `Docker`_ manifests used for building the images publish on
`Docker Hub`_ in the official `BuildGrid organization`_. BuildGrid's own
development happens in a `separate repository`_.

.. _Remote Execution API: https://github.com/bazelbuild/remote-apis/tree/master/build/bazel/remote/execution/v2
.. _Remote Workers API: https://github.com/googleapis/googleapis/tree/master/google/devtools/remoteworkers/v1test2
.. _Docker: https://www.docker.com
.. _Docker Hub: https://hub.docker.com
.. _BuildGrid organization: https://hub.docker.com/u/buildgrid
.. _separate repository: https://gitlab.com/BuildGrid/buildgrid


.. _how-to-use-ci:

What's the CI doing?
--------------------

Nightly builds
~~~~~~~~~~~~~~

The main purpose of this repository is to periodically build (daily, at 4:00am
UTC) nightly Docker images for BuildGrid and buildbox and publish them. So far
that includes:

- `buildgrid:nightly`_: nightly image for BuildGrid, published at:

  * GitLab registry: ``registry.gitlab.com/buildgrid/buildgrid.hub.docker.com/buildgrid:nightly``

- `buildbox:nightly`_: nightly image for buildbox, published at:

  * GitLab registry: ``registry.gitlab.com/buildgrid/buildgrid.hub.docker.com/buildbox:nightly``

.. _buildgrid:nightly: https://gitlab.com/BuildGrid/buildgrid.hub.docker.com/blob/master/buildgrid/Dockerfile.nightly
.. _buildbox:nightly: https://gitlab.com/BuildGrid/buildgrid.hub.docker.com/blob/master/buildbox/Dockerfile.nightly

End-to-end testing
~~~~~~~~~~~~~~~~~~

Besides nightly builds, this repository also provides configured CI pipelines
for remote execution end-to-end testing. The goal is to provide one test job for
every element in the remote execution chain (client, controller, storage, bot).
Each job builds one element from sources and test it against the latest stable
version of all the others. So far that includes:

- BuildStream: client test, job steps:

  * Builds BuildStream from sources (`buildstream/Dockerfile.test`_).
  * Spins-up BuildGrid grid (`Composefile.buildstream.yml`_).
  * Runs a test build (simple `autotools project`_).

- RECC: client test, job steps:

  * Builds RECC from sources (`recc/Dockerfile.test`_).
  * Spins-up BuildGrid grid (`Composefile.recc.yml`_).
  * Runs a test build (simple `amhello project`_).

.. _buildstream/Dockerfile.test: https://gitlab.com/BuildGrid/buildgrid.hub.docker.com/blob/master/buildstream/Dockerfile.test
.. _Composefile.buildstream.yml: https://gitlab.com/BuildGrid/buildgrid.hub.docker.com/blob/master/Composefile.buildstream.yml
.. _autotools project: https://gitlab.com/BuildStream/buildstream/tree/master/doc/examples/autotools
.. _recc/Dockerfile.test: https://gitlab.com/BuildGrid/buildgrid.hub.docker.com/blob/master/recc/Dockerfile.test
.. _Composefile.recc.yml: https://gitlab.com/BuildGrid/buildgrid.hub.docker.com/blob/master/Composefile.recc.yml
.. _amhello project: http://git.savannah.gnu.org/cgit/automake.git/tree/doc/amhello


.. _how-to-use-it:

How to spin-up a grid?
----------------------

Most of the tooling in this repository (Docker and Docker Compose manifests) is
used as part of automated CI pipelines. Some of it can be used manually though
and may help you spinning-up a grid locally.

For BuildStream
~~~~~~~~~~~~~~~

`Docker Compose up`_ invocation:

.. code-block:: sh

  docker-compose -f Composefile.buildstream.yml up --scale bots=2

`BuildStream configuration`_ snippet:

.. code-block:: yaml

  artifacts:
    url: http://localhost:50052
    push: true

  remote-execution:
    execution-service:
      url: http://localhost:50051
    action-cache-service:
      url: http://localhost:50052
    storage-service:
      url: http://localhost:50052

.. _Docker Compose up: https://docs.docker.com/v17.09/compose/reference/up
.. _BuildStream configuration: https://buildstream.gitlab.io/buildstream/using_config.html#remote-execution

For RECC
~~~~~~~~

Docker Compose up invocation:

.. code-block:: sh

  docker-compose -f Composefile.recc.yml up --scale bots=4

`RECC configuration`_ snippet:

.. code-block:: ini

  server = localhost:50051
  action_cache_server = localhost:50052
  cas_server = localhost:50052

.. _RECC configuration: https://gitlab.com/bloomberg/recc#configuration
